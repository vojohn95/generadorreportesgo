module generadorreportesgo

go 1.14

require (
	github.com/360EntSecGroup-Skylar/excelize v1.4.1
	github.com/go-sql-driver/mysql v1.5.0
	github.com/schollz/progressbar v1.0.0
)
