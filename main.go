package main

import (
	"database/sql"
	"fmt"
	"github.com/360EntSecGroup-Skylar/excelize"
	_ "github.com/360EntSecGroup-Skylar/excelize"
	_ "github.com/go-sql-driver/mysql"
	"github.com/schollz/progressbar"
	"log"
	"strconv"
	"time"
)

func main() {
	fmt.Println("******COMIENZA PROGRAMA*******")

	t := time.Now()
	//fmt.Println(t.String())
	fmt.Printf("Fecha hoy: ")
	fmt.Println(t.Format("2006-01-02"))
	fmt.Printf("Dia anterior: ")
	fecha2 := t.Add(365*24 - (24 * time.Hour))
	//fecha2 := "2019-11-13"
	fmt.Println(fecha2.Format("2006-01-02"))
	//fechaini := fecha2.String()
	//fmt.Printf()
	//hostinger
	//conexion := "u548444544_factn:Integrador1@tcp(sql205.main-hosting.eu:3306)/u548444544_factn"
	//produccion
	conexion := "devengers:F@ctC3ntr41@tcp(201.161.84.233:3306)/central_aplicativo"

	db, err := sql.Open("mysql", conexion)

	defer Recuperacion(conexion)
	if err != nil {
		panic(err.Error())
		log.Print(err)
	}

	defer db.Close()

	fmt.Println("Conectado a la base destino")

	consulta, err := db.Query("SELECT B.folio, B.serie, B.uuid, B.FECHA_TIMBRADO,  A.Razon_social, round((B.total_factura / 1.16),2)subtotal_factura, round(((B.total_factura / 1.16)*0.16),2)iva_factura, 0,  0,  B.total_factura, B.total_factura, 0,  'Pagada' FROM central_aplicativo.autof_tickets as A, central_aplicativo.auto_facts as B WHERE A.id = B.id_ticketAF and B.created_at BETWEEN ?  AND ? AND B.UUID <> '' ", fecha2.Format("2006-01-02")+" 00:00:00", fecha2.Format("2006-01-02")+" 23:59:59")
	//consulta, err := db.Query("SELECT B.folio, B.serie, B.uuid, B.FECHA_TIMBRADO,  A.Razon_social, round((B.total_factura / 1.16),2)subtotal_factura, round(((B.total_factura / 1.16)*0.16),2)iva_factura, 0,  0,  B.total_factura, B.total_factura, 0,  'Pagada' FROM central_aplicativo.autof_tickets as A, central_aplicativo.auto_facts as B WHERE A.id = B.id_ticketAF and B.created_at BETWEEN ?  AND ? AND B.UUID <> '' ", "2019-11-13"+" 00:00:00", "2019-11-13"+" 23:59:59")
		defer Recuperacion(conexion)
		if err != nil {
			panic(err.Error())
			log.Print(err)
		}

	rows, err := db.Query("SELECT count(*) FROM central_aplicativo.autof_tickets as A, central_aplicativo.auto_facts as B WHERE A.id = B.id_ticketAF and B.created_at BETWEEN ? AND ? AND B.UUID <> '' ", fecha2.Format("2006-01-02")+" 00:00:00", fecha2.Format("2006-01-02")+" 23:59:59")
	//rows, err := db.Query("SELECT count(*) FROM central_aplicativo.autof_tickets as A, central_aplicativo.auto_facts as B WHERE A.id = B.id_ticketAF and B.created_at BETWEEN ? AND ? AND B.UUID <> '' ", "2019-11-13"+" 00:00:00", "2019-11-13"+" 23:59:59")
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()

	var count int

	for rows.Next() {
		if err := rows.Scan(&count); err != nil {
			log.Fatal(err)
		}
	}

	fmt.Println("Cantidad de registros %i\n", count)

	fmt.Println("Comienza consulta")

	//comienza generacion de excel
	xlsx := excelize.NewFile()
	// Create a new sheet.
	index := xlsx.NewSheet("Sheet1")
	// Set value of a cell.
	xlsx.SetCellValue("Sheet1", "A1", "folio")
	xlsx.SetCellValue("Sheet1", "B1", "serie")
	xlsx.SetCellValue("Sheet1", "C1", "uuid")
	xlsx.SetCellValue("Sheet1", "D1", "fecha_timbrado")
	xlsx.SetCellValue("Sheet1", "E1", "Razon social")
	xlsx.SetCellValue("Sheet1", "F1", "Subtotal_factura")
	xlsx.SetCellValue("Sheet1", "G1", "iva_factura")
	xlsx.SetCellValue("Sheet1", "H1", "0")
	xlsx.SetCellValue("Sheet1", "I1", "0")
	xlsx.SetCellValue("Sheet1", "J1", "total_factura")
	xlsx.SetCellValue("Sheet1", "K1", "total_factura")
	xlsx.SetCellValue("Sheet1", "L1", "0")
	xlsx.SetCellValue("Sheet1", "M1", "Pagada")


	sum := 2

	bar := progressbar.New(100)

	for consulta.Next() {
		//var folio int
		//var monto float32
		var folio, serie, uuid, Fecha_timbrado, Razon_social, subtotal, iva_factura, cero, cero1, total_factura, total_facturab, cero2, pagada string
		//var Fecha uint8

		//strconv.Itoa(int(Fecha))
		defer Recuperacion(conexion)
		if err := consulta.Scan(&folio, &serie, &uuid, &Fecha_timbrado, &Razon_social, &subtotal, &iva_factura, &cero, &cero1, &total_factura, &total_facturab, &cero2, &pagada)
			err != nil {
			log.Fatal(err.Error())
		} //termina err

		//fmt.Printf("datos encontrados = %s - %s - %v - %v - %v - %v - %v - %v -%v - %v - %v -%v -%v \n", folio, serie, uuid, Fecha_timbrado, Razon_social, subtotal, iva_factura, cero, cero1, total_factura, total_facturab, cero2, pagada)
		//for sum < count {
			y := strconv.Itoa(sum)
			xlsx.SetCellValue("Sheet1", "A"+y, folio)
			xlsx.SetCellValue("Sheet1", "B"+y, serie)
			xlsx.SetCellValue("Sheet1", "C"+y, uuid)
			xlsx.SetCellValue("Sheet1", "D"+y, Fecha_timbrado)
			xlsx.SetCellValue("Sheet1", "E"+y, Razon_social)
			xlsx.SetCellValue("Sheet1", "F"+y, subtotal)
			xlsx.SetCellValue("Sheet1", "G"+y, iva_factura)
			xlsx.SetCellValue("Sheet1", "H"+y, "0")
			xlsx.SetCellValue("Sheet1", "I"+y, "0")
			xlsx.SetCellValue("Sheet1", "J"+y, total_factura)
			xlsx.SetCellValue("Sheet1", "K"+y, total_factura)
			xlsx.SetCellValue("Sheet1", "L"+y, "0")
			xlsx.SetCellValue("Sheet1", "M"+y, "Pagada")
			sum += 1
		//}

			bar.Add(1)
			//time.Sleep(10 * time.Millisecond)

	}

	xlsx.SetActiveSheet(index)

	path := "reportes/" + fecha2.Format("2006-01-02") + ".xlsx"
	//path := "reportes/" + "fecha2.Format("2006-01-02")" + ".xlsx"
	// Save xlsx file by the given path.
	errr := xlsx.SaveAs(path)
	defer Recuperacion("creación de archivo")
	if errr != nil {
		panic(errr.Error())
		log.Print(errr)
	}

	fmt.Println()
	fmt.Println("Finalizando aplicación....")
	time.Sleep(time.Second * 3)
	fmt.Println("Busque en la carpeta reportes su excel")
	time.Sleep(time.Second * 2)
	fmt.Println("El programa se cerrara automaticamente")
	time.Sleep(time.Second * 5)

} //termina main


func Recuperacion(IP string) {
	recuperado := recover()
	if recuperado != nil {
		fmt.Println("Recuperación de: ", IP, recuperado)
	}
}
